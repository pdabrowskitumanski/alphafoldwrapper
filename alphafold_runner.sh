cd /app/alphafold
/app/run_alphafold.sh \
 --fasta_paths=${FASTA} \
 --uniref90_database_path=${DB}/uniref90/uniref90.fasta \
 --uniref30_database_path=${DB}/uniref30/UniRef30_2021_03 \
 --mgnify_database_path=${DB}/mgnify/mgy_clusters_2022_05.fa \
 --pdb70_database_path=${DB}/pdb70/pdb70 \
 --data_dir=${DB} \
 --template_mmcif_dir=${DB}/pdb_mmcif/mmcif_files \
 --obsolete_pdbs_path=${DB}/pdb_mmcif/obsolete.dat \
 --bfd_database_path=${DB}/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt \
 --output_dir=${OUT_PATH} \
 --max_template_date=2023-06-01 \
 --db_preset=full_dbs \
 --benchmark=False \
 --use_gpu_relax=True \
 --model_preset=monomer \
 --logtostderr