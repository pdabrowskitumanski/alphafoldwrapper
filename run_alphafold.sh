protein_name=$1
protein_variant=$2

dsub --provider google-cls-v2 \
 --project ${PROJECT_NAME} \
 --zones us-central1-a \
 --logging gs://${DATA_BUCKET}/logs \
 --image=gcr.io/${PROJECT_NAME}/alphafold:latest \
 --script=bin/alphafold_runner.sh \
 --input FASTA=gs://${DATA_BUCKET}/input/${protein_name}/${protein_variant}.fasta \
 --mount DB=${ALPHAFOLD_DATA_IMAGE} \
 --output-recursive OUT_PATH=gs://{DATA_BUCKET}/output/${protein_name} \
 --machine-type n1-standard-8 \
 --boot-disk-size 100 \
 --accelerator-type nvidia-tesla-k80 \
 --accelerator-count 1