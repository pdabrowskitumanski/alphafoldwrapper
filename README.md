# AlphaFold Wrapper
![image](https://img.shields.io/badge/Google_Cloud-4285F4?style=plastic&logo=google-cloud&logoColor=white)
![image](https://img.shields.io/badge/AlphaFold-2.3.2-blue?style=plastic)

For easy job running on Google Cloud Platform
Data for manuscript "Alphafold blindness to topological barriers affects its ability to
correctly predict proteins’ topology"

## Idea
AlphaFold is a powerful software predicting the protein structure out of the sequence.
However, due to it's construction it is blind to topological constraints.
In particular, it should be impossible to obtain a chain with three consecutive knots on one protein chain.
To test this, we created artificial sequences multiplying knotted protein domains.
It turns out, that AlphaFold wrongly predicts structures with 3, 5 or even 10 consecutive knots without any problems.
This repository contains files and scripts needed to recreate our results. 

## Setting up 
To set up the project you have to
1. Have a Google Cloud Platform account
1. Increase the limit of GPU (by default it is 0)
1. Create a Docker container for AlphaFold code
1. Download all the data for AlphaFold and make it available as disk image
1. Enable Life Sciences API
1. Prepare environment
Points 3-6 are described in [this Medium post](https://medium.com/google-cloud/running-alphafold-with-google-cloud-life-sciences-7219db6ca99e)

## Running AlphaFold on GCP
It all is set up, please fill in the missing parameters (`PROJECT_NAME`, `DATA_BUCKET`, `ALPHAFOLD_DATA_IMAGE`, `ZONE`) in `env.sh` file.
To model a protein, please put a desired sequence on the bucket in the directory `sequences/protein_name/variant` were `protein_name` may be any string, like PDB code, `variant` is a version of this protein, like `x3`.
Next, in the GCP Cloud Shell type:
```
source env.sh
./run_alphafold.sh protein_name variant
```
Wait for the results.

### Costs
The highest cost if of course computations. It takes about 15$/protein, but it depends heavily on protein length. 
On Google Cloud one obtains a free credits of 300$ on start. 
Also, one has to take into account the storage - keeping the image of the AlphaFold data costs around 2$ per day, independently if the are any calculations or not.
To reduce the costs, one can also use preemptible instances, which can reduce the cost up to 90%.
To do so, add `--preemptible` as a last line in `run_alphafold.sh` script.
Remember, 'preemptible' means, that such instances can be emptied with no warning, so if there is higher demand on computational power in the availability zone, most probably the AlphaFold calculations will not finish properly.

## Protein models
All sequences used in the study are stored in `sequences.csv` file. It contains pdb code of the original structure, the multiplicity of the domains, the linker used, final sequence, and obtained topology and mean pLDDT of the best model.
The topology is given in a non-rigorous, yet hopefully understandable notation. 
The file contains also the names of the models obtained from AlphaFold.
The model files can be found in the directory `models`.
