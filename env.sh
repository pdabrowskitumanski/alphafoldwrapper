export PROJECT_NAME="<put here your GCP project name>"
export DATA_BUCKET="<put here your GCP bucket with data>"
export ALPHAFOLD_DATA_IMAGE="<put here link to your GCP image with ALphaFold Data>"
export ZONE="<put here your calculation zone>"

source env/bin/activate

